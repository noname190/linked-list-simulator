import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
/*
 * Class does all the work for the panel
 */
public class SimulatorPanel extends JPanel {

	Truck truck;
	ArrayList<Car> cars;
	Car selectedCar;
	int last_x;
	int last_y;

	/*
	 * Contruscter that instantiates listeners and arraylist
	 */
	public SimulatorPanel() {
		MouseListener listener = new MousePressListener();
		addMouseListener(listener);
		MouseMotionListener mListener = new MousePressListener();
		addMouseMotionListener(mListener);
		cars = new ArrayList<Car>();
		

	}

	/*
	 * Mouse adapter class
	 */
	class MousePressListener extends MouseAdapter {

		/*
		 * When mouse is pressed then create cars
		 * if cars are created and clicked on a car then create a new car
		 * @param arg0 the mouse event
		 *  */
		@Override
		public void mousePressed(MouseEvent arg0) {
			System.out.println("click");
			if (selectedCar != null) {
				selectedCar.deSelect();
				selectedCar = null;
			}
			for (Car car : cars)
				car.deSelect();
			repaint();
			if (truck == null)
				truck = new Truck(arg0.getX(), arg0.getY());
			else if (cars.size() < 5)
				cars.add(new Car(arg0.getX(), arg0.getY()));
			else

				for (Car car : cars) {
					if (!isTrailer(car))
						if (car.contains(arg0.getX(), arg0.getY())) {

							car.selected();
							selectedCar = car;
							last_x = selectedCar.getX() - arg0.getX();
							last_y = selectedCar.getY() - arg0.getY();
							selectedCar.updateLocation(arg0, last_x, last_y);
							repaint();
						}
				}
			repaint();

		}

		/*
		 * When mouse is dragged then repaint selected car
		 * @param arg0 the mouse event
		 * */
		@Override
		public void mouseDragged(MouseEvent arg0) {

			if (selectedCar != null) {
				
				selectedCar.updateLocation(arg0, last_x, last_y);
				repaint();

			}

		
		}

		/*
		 * Called when mouse is relased and if on top of another car then it becomes the trailer
		 * @param e the mouseevent
		 *  
		 *  */
		public void mouseReleased(MouseEvent e) {
			if (selectedCar != null) {
				if (selectedCar.contains(truck)) {
				
					truck.addLast(selectedCar);
					repaint();
					return;
				}
				for (Car car : cars) {
					if (selectedCar.contains(car) && car != selectedCar) {
						if (car.trailer == null) {
							car.trailer = selectedCar;
							repaint();
							return;
						}
						
						else if(selectedCar.trailer != car && !isTrailer(car)){
							selectedCar.addLast(car);
							repaint();
							return;
						}
						

					}
				}

			
			}
		}

	}

	/*
	 *The draw method that draws the cars and truck
	 *@param g the graphics context
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		// g2.setPaint(getBackground());
		// g2.fillRect(0, 0, getWidth(), getHeight());
		if (truck != null)
			truck.draw(g2);
		if (cars.size() > 0)
			for (Car car : cars)
				if (car.trailer == null && !isTrailer(car))
					car.draw(g2);
				else if (!isTrailer(car) && !car.disappear)
					car.drawRecursive(g2);

	}

	/*
	 * Add car to beginning of the truck
	 */
	public void addFirstItem() {
		if (selectedCar != null)
			truck.addFirst(selectedCar);
		repaint();

	}

	/*
	 * Add car to end of the truck
	 */
	public void addLastItem() {
		if (selectedCar != null)
			truck.addLast(selectedCar);
		repaint();
	}

	/*
	 * Removes first car from truck
	 */
	public void removeFirstItem() {
		if (truck.trailer != null)
			truck.removeFirstItem();
		repaint();

	}

	/*
	 * Checks if car is a trailer and returns
	 * @param c the car to check if trailer
	 * @return true is truck is trailer
	 */
	public boolean isTrailer(Car c) {
		Car l = (Car) truck.trailer;
		if(l!=null)
		if(l.number==c.number)return true;
		for (Car car : cars) {
			Car t = (Car) car.trailer;
			Car tr = (Car)  truck.trailer;
			
			
			if (t != null)
				if (t.number == c.number && t != null) {
					System.out.println("trailer");
					return true;
				} else if(tr!=null){if(tr.number == c.number) return true;}
				else
					continue;
		}
		return false;

	}

	/*
	 * Remove last car from truck
	 */
	public void removeLastItem() {
		
		if (truck.trailer != null)
			truck.removeLastItem();
		repaint();

	}

	/*
	 * Move truck left
	 */
	public void moveLeft() {
		truck.moveLeft();
		repaint();

	}

	/*
	 * Move truck right
	 */
	public void moveRight() {
		truck.moveRight();
		repaint();
	}

	/*
	 * Move truck up
	 */
	public void moveUp() {
		truck.moveUp();
		repaint();

	}

	/*
	 * Move truck down
	 */
	public void moveDown() {
		truck.moveDown();
		
		repaint();

	}

	/*
	 * Moves the truck randomly
	 */
	public void moveRandom(int width, int height) {
		truck.moveRandom(width - 10, height - 20);
		repaint();

	}

	/*
	 * 
	 * Resets the panel
	 */
	public void resetArray() {
		cars = new ArrayList<Car>();
		truck = null;
		Car.carNumber = 1;
		repaint();
	}
}
