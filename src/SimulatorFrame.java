import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MenuBar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * This frame has a menu with commands to change the font of a text sample.
 */
public class SimulatorFrame extends JFrame {
	static final int FRAME_WIDTH = 300;
	static final int FRAME_HEIGHT = 400;

	SimulatorPanel panel;

	/**
	 * Constructs the frame.
	 */
	public SimulatorFrame() {
		// Construct text sample

		// Construct menu
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		menuBar.add(createFileMenu());
		menuBar.add(createEditMenu());
		menuBar.add(createListMenu());
		panel = new SimulatorPanel();
		add(panel);

		setSize(FRAME_WIDTH, FRAME_HEIGHT);

	}

	/**
	 * Resets the panel
	 */
	public void newFrame() {
		panel.resetArray();
		
	}

	/**
	 * Exit item Listener class
	 *
	 */
	class ExitItemListener implements ActionListener {
		/*
		 * Called when button is clicked.
		 * @param event data for this event
		 */
		public void actionPerformed(ActionEvent event) {
			System.exit(0);//exit the program
		}
	}

	/**
	 * Creates the File menu.
	 * 
	 * @return the menu
	 */
	public JMenu createFileMenu() {
		JMenu menu = new JMenu("File");
		JMenuItem exitItem = new JMenuItem("Exit");
		ActionListener listener = new ExitItemListener();
		exitItem.addActionListener(listener);
		JMenuItem newItem = new JMenuItem("New");
		ActionListener nlistener = new NewItemListener();
		newItem.addActionListener(nlistener);
		menu.add(newItem);
		menu.add(exitItem);
		return menu;
	}

	class NewItemListener implements ActionListener {
		/*
		 * Called when button is clicked.
		 * @param event data for this event
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			newFrame();
			repaint();
		}
	}

	/**
	 * Creates the Font submenu.
	 * 
	 * @return the menu
	 */
	public JMenu createEditMenu() {
		JMenu menu = new JMenu("Edit");
		menu.add(createMoveMenu());
		JMenuItem randomizeItem = new JMenuItem("Randomize");
		menu.add(randomizeItem);
		ActionListener rlistener = new RandomItemListener();
		randomizeItem.addActionListener(rlistener);
		return menu;
	}

	/*
	 * Creates the Move submenu
	 * @return the menu
	 */
	public JMenu createMoveMenu() {
		JMenu menu = new JMenu("Move");
		JMenuItem leftItem = new JMenuItem("Left");
		JMenuItem rightItem = new JMenuItem("Right");
		JMenuItem upItem = new JMenuItem("Up");
		JMenuItem downItem = new JMenuItem("Down");
		menu.add(leftItem);
		menu.add(rightItem);
		ActionListener rlistener = new MoveRightItemListener();
		rightItem.addActionListener(rlistener);
		menu.add(upItem);
		ActionListener ulistener = new MoveUpItemListener();
		upItem.addActionListener(ulistener);
		ActionListener dlistener = new MoveDownItemListener();
		downItem.addActionListener(dlistener);
		menu.add(downItem);
		ActionListener mlistener = new MoveLeftItemListener();
		leftItem.addActionListener(mlistener);
		return menu;
	}

	class MoveLeftItemListener implements ActionListener {
		/*
		 * Called when button is clicked.
		 * @param event data for this event
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			panel.moveLeft();

		}

	}

	class MoveRightItemListener implements ActionListener {
		/*
		 * Called when button is clicked.
		 * @param event data for this event
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			panel.moveRight();

		}

	}

	class MoveUpItemListener implements ActionListener {
		/*
		 * Called when button is clicked.
		 * @param event data for this event
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			panel.moveUp();

		}

	}

	class MoveDownItemListener implements ActionListener {
		/*
		 * Called when button is clicked.
		 * @param event data for this event
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			panel.moveDown();
			System.out.println("down");

		}

	}
	class RandomItemListener implements ActionListener {
		/*
		 * Called when button is clicked.
		 * @param event data for this event
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			panel.moveRandom(getWidth(), getHeight());

		}

	}


	/**
	 * Creates the Font submenu.
	 * 
	 * @return the menu
	 */
	public JMenu createListMenu() {
		JMenu menu = new JMenu("List");
		JMenuItem addFirstItem = new JMenuItem("Add First");
		JMenuItem addLastItem = new JMenuItem("Add Last");
		JMenuItem removeFirstItem = new JMenuItem("Remove First");
		JMenuItem removeLastItem = new JMenuItem("Remove Last");
		menu.add(addFirstItem);
		menu.add(addLastItem);
		menu.add(removeFirstItem);
		menu.add(removeLastItem);
		ActionListener fListener = new FirstItemListener();
		addFirstItem.addActionListener(fListener);
		ActionListener lListener = new LastItemListener();
		addLastItem.addActionListener(lListener);
		ActionListener rfListener = new RemoveFirstItemListener();
		removeFirstItem.addActionListener(rfListener);
		ActionListener rlListener = new RemoveLastItemListener();
		removeLastItem.addActionListener(rlListener);
		return menu;
	}

	class FirstItemListener implements ActionListener {
		/*
		 * Called when button is clicked.
		 * @param event data for this event
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			panel.addFirstItem();

		}

	}

	class LastItemListener implements ActionListener {
		/*
		 * Called when button is clicked.
		 * @param event data for this event
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			panel.addLastItem();

		}

	}

	class RemoveFirstItemListener implements ActionListener {
		/*
		 * Called when button is clicked.
		 * @param event data for this event
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			panel.removeFirstItem();

		}

	}

	class RemoveLastItemListener implements ActionListener {
		/*
		 * Called when button is clicked.
		 * @param event data for this event
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			panel.removeLastItem();

		}
	}

}
