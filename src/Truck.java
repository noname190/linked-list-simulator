import java.awt.event.MouseEvent;
import java.awt.geom.*;
import java.awt.*;
import java.util.Random;

/**
 * Truck is a vehicle that can pull a chain of cars
 */
public class Truck extends Vehicle {
	private int xLeft;
	private int yTop;

	/**
	 * Constants
	 */
	private static final double WIDTH = 35;
	private static final double UNIT = WIDTH / 7;
	private static final double LENGTH_FACTOR = 14; // length is 14U
	private static final double HEIGHT_FACTOR = 5; // height is 5U
	private static final double U_3 = 0.3 * UNIT;
	private static final double U2_5 = 2.5 * UNIT;
	private static final double U3 = 3 * UNIT;
	private static final double U4 = 4 * UNIT;
	private static final double U5 = 5 * UNIT;
	private static final double U10 = 10 * UNIT;
	private static final double U10_7 = 10.7 * UNIT;
	private static final double U12 = 12 * UNIT;
	private static final double U13 = 13 * UNIT;
	private static final double U14 = 14 * UNIT;

	/**
	 * Constructs truck at position
	 * 
	 * @param x
	 *            the x position
	 * @param y
	 *            the y position
	 */
	public Truck(int x, int y) {
		xLeft = x;
		yTop = y;
		boundingBox = new Rectangle(xLeft, yTop, (int) (U3 + U10 + 4),
				(int) (U4 + UNIT));
	}

	/**
	 * Draws the truck
	 * 
	 * @param g2
	 *            the graphics context
	 */
	public void draw(Graphics2D g2) {
		int x1 = xLeft;
		int y1 = yTop;
		Rectangle2D.Double hood = new Rectangle2D.Double(x1, y1 + UNIT, U3, U3);
		g2.setColor(Color.red);
		g2.fill(hood);

		Rectangle2D.Double body = new Rectangle2D.Double(x1 + U3, y1, U10, U4);
		g2.setColor(Color.blue);
		g2.fill(body);

		Line2D.Double hitch = new Line2D.Double(x1 + U3 + U10, y1 + U4 / 2, x1
				+ U3 + U10 + 4, y1 + U4 / 2);
		g2.setColor(Color.black);
		g2.fill(hitch);

		Ellipse2D.Double wheel1 = new Ellipse2D.Double(x1 + U_3, y1 + U4, UNIT,
				UNIT);
		g2.setColor(Color.black);
		g2.fill(wheel1);

		Ellipse2D.Double wheel2 = new Ellipse2D.Double(x1 + U3, y1 + U4, UNIT,
				UNIT);
		g2.setColor(Color.black);
		g2.fill(wheel2);

		Ellipse2D.Double wheel3 = new Ellipse2D.Double(x1 + 4 * UNIT, y1 + 4
				* UNIT, UNIT, UNIT);
		g2.setColor(Color.black);
		g2.fill(wheel3);

		Ellipse2D.Double wheel4 = new Ellipse2D.Double(x1 + U10_7, y1 + U4,
				UNIT, UNIT);
		g2.setColor(Color.black);
		g2.fill(wheel4);

		Ellipse2D.Double wheel5 = new Ellipse2D.Double(x1 + U12, y1 + U4, UNIT,
				UNIT);
		g2.setColor(Color.black);
		g2.fill(wheel5);

		if (trailer != null) {
			trailer.draw(g2, xLeft + (int) (U3 + U10 + 4), yTop);
		}

	}

	/*
	 * Adds selectedCar to the trailer
	 * @param selectedCar the car to add to the front
	 */
	public void addFirst(Car selectedCar) {
		
		//if not trailer
		if (trailer == null)
			trailer = selectedCar;
		if (selectedCar.equals(trailer))
			return;
		//set as trailer and update trailer of car
		if (trailer != null) {
			
			Vehicle temp = selectedCar;
			while (temp.trailer != null) {
				temp = temp.trailer;
			}
			temp.trailer = trailer;
			trailer = selectedCar;
		}

	}

	/*
	 * Adds selectedCar to the last position
	 * @param selecetedCar the car to add
	 */
	public void addLast(Car selectedCar) {

		if (trailer == null) {
			trailer = selectedCar;
			return;
		}

		Car temp = (Car) trailer;
		while (temp.trailer != null)
			temp = (Car) temp.trailer;

		Car t = (Car) temp;
		if (t != null)
			if (t.number == selectedCar.number)
				return;
		temp.trailer = selectedCar;

	}

	/*
	 * removes the first car
	 */
	public void removeFirstItem() {
		if (trailer.trailer == null) {
			((Car) trailer).disappear();
			trailer = null;
			return;
		}
		((Car) trailer).disappear();
		trailer = trailer.trailer;

	}

	/*
	 * Removes the last car
	 */
	public void removeLastItem() {
		
		if (trailer == null) {
			return;
		} else if (trailer.trailer == null) {
			((Car) trailer).disappear();
			trailer = null;
		} else {
			Vehicle temp = trailer;
			while (temp.trailer != null) {
				if (temp.trailer.trailer == null) {
					((Car) temp.trailer).disappear();
					temp.trailer = null;
					return;
				}
				temp = temp.trailer;

			}

			temp = null;
			// temp = null;
		}

	}

	/*
	 * Moves the truck left
	 */
	public void moveLeft() {
		updateLocation(xLeft - 20, yTop);

	}

	/*
	 * Updates the  truck location
	 * @param x the x location to update to
	 * @param y the y location to update to
	 */
	public void updateLocation(int x, int y) {
		xLeft = x;
		yTop = y;

	}

	/*
	 * Move the truck right
	 */
	public void moveRight() {
		updateLocation(xLeft + 20, yTop);
	}

	/*
	 * Move the truck up
	 */
	public void moveUp() {
		updateLocation(xLeft, yTop - 20);

	}
	
	/*
	 * Move the truck down
	 */
	public void moveDown() {
		updateLocation(xLeft, yTop + 20);
		

	}

	/*
	 * Move the truck randomly
	 * @param width the width of where to randomize the car
	 * @param height the height of which to randomize the car
	 */
	public void moveRandom(int width, int height) {
		Random random = new Random();
		updateLocation(random.nextInt(width) - 20, random.nextInt(height) - 20);
	}

}
