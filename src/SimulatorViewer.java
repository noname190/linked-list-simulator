
import javax.swing.JFrame;

/**
   This program uses a menu to display font effects.
*/
public class SimulatorViewer
{  
   public static void main(String[] args)
   {  
      JFrame frame = new SimulatorFrame();
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setTitle("Linked List Simulator");
      frame.setVisible(true);      

   }
}

 