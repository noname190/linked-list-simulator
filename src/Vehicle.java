import java.awt.Graphics2D;
import java.awt.Rectangle;

/*
 * Vehicle super class
 */
abstract public class Vehicle {
	protected Vehicle trailer;
	protected Rectangle boundingBox;

	/*
	 * Returns true if vehicle contains x,y cordinates
	 * 
	 * @param x the x cordinate
	 * 
	 * @param y the y cordinate
	 * 
	 * @return if vehicles contain each other
	 */
	public boolean contains(double x, double y) {
		return boundingBox.contains(x, y);
	}

	/*
	 * Return the bounding box return bouding box rectangle
	 */
	public Rectangle getBoundingBox() {
		return boundingBox;
	}

	/*
	 * Returns true if vehicles contain each other
	 * 
	 * @param v the vehicle to compare with
	 * 
	 * @return wether vehicles contain
	 */
	public boolean contains(Vehicle v) {
		if (v.trailer == null)
			return boundingBox.intersects(v.getBoundingBox());
		else
			return getBoundingBoxTrailer2().intersects(
					v.getBoundingBoxTrailer());
	}

	/*
	 * Returns the bounding box but including the lenght of the trailers
	 */
	private Rectangle getBoundingBoxTrailer() {
		Vehicle temp = trailer;
		Rectangle u;
		int count = 1;

		while (temp.trailer != null) {
			temp = temp.trailer;
			count++;
		}
		u = new Rectangle((int) boundingBox.getX(), (int) boundingBox.getY(),
				(int) count * (int) boundingBox.getWidth(),
				(int) boundingBox.getHeight());
		return u;
	}

	/*
	 * Returns the bounding box but only for one car and one trailer
	 */
	private Rectangle getBoundingBoxTrailer2() {
		if (trailer == null)
			return boundingBox;
		Vehicle temp = trailer;
		Rectangle u;
		int count = 2;
		while (temp.trailer != null) {
			temp = temp.trailer;
			count++;
		}
		u = new Rectangle((int) boundingBox.getX(), (int) boundingBox.getY(),
				(int) count * (int) boundingBox.getWidth(),
				(int) boundingBox.getHeight());
		return u;
	}

	/*
	 * Draw method 
	 * @param g2 draw context
	 * @param i
	 * @param yTop the y cordinate
	 */
	public void draw(Graphics2D g2, int i, int yTop) {

	}

	/*
	 * Draw method in red color
	 * @param g2 draw context
	 * @param i 
	 * @param yTop the y cordinate
	 */
	public void drawRedRecur(Graphics2D g2, int i, int yTop) {

	}

}
