import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * A car shape that can be positioned anywhere on the screen.
 */
public class Car extends Vehicle {
	int xLeft;
	int yTop;
	static int carNumber = 1;//static car number that is incremented every time a car is made
	int number;//cars number
	boolean selected = false;
	boolean disappear = false;

	/**
	 * Constructs a car with a given top left corner.
	 * 
	 * @param x
	 *            the x coordinate of the top left corner
	 * @param y
	 *            the y coordinate of the top left corner
	 */
	public Car(int x, int y) {
		xLeft = x;
		yTop = y;
		number = carNumber;
		carNumber++;
	}

	
	/**
	 * Draws the car and all of it's features
	 * @param g2 the Graphics2D variable to draw on
	 */
	public void draw(Graphics2D g2) {
		if (!disappear) {
			boundingBox = new Rectangle(xLeft, yTop, 64, 30);

			if (selected)
				g2.setColor(Color.red);
			else
				g2.setColor(Color.black);
			Rectangle body = new Rectangle(xLeft, yTop + 10, 60, 10);
			Ellipse2D.Double frontTire = new Ellipse2D.Double(xLeft + 10,
					yTop + 20, 10, 10);
			Ellipse2D.Double rearTire = new Ellipse2D.Double(xLeft + 40,
					yTop + 20, 10, 10);

			Line2D.Double hitch = new Line2D.Double(xLeft + 60, yTop + 15,
					xLeft + 64, yTop + 15);

			// The bottom of the front windshield
			Point2D.Double r1 = new Point2D.Double(xLeft + 10, yTop + 10);
			// The front of the roof
			Point2D.Double r2 = new Point2D.Double(xLeft + 20, yTop);
			// The rear of the roof
			Point2D.Double r3 = new Point2D.Double(xLeft + 40, yTop);
			// The bottom of the rear windshield
			Point2D.Double r4 = new Point2D.Double(xLeft + 50, yTop + 10);

			Line2D.Double frontWindshield = new Line2D.Double(r1, r2);
			Line2D.Double roofTop = new Line2D.Double(r2, r3);
			Line2D.Double rearWindshield = new Line2D.Double(r3, r4);

			g2.drawString(Integer.toString(number), xLeft + 30, yTop + 20);
			g2.draw(body);
			g2.draw(frontTire);
			g2.draw(rearTire);
			g2.draw(frontWindshield);
			g2.draw(roofTop);
			g2.draw(rearWindshield);
			g2.draw(hitch);
			// g2.draw(boundingBox);

		}
	}

	/**
	 * Sets the selected variable to true
	 */
	public void selected() {

		selected = true;
	}

	/**
	 * Lets the car object knows its deselected
	 */
	public void deSelect() {
		selected = false;
	}

	/**
	 * Updates the location for the car
	 * @param e the mouse event variable
	 * @param lastx the last x location for the car
	 * @param lasty the last y location for the car
	 */
	public void updateLocation(MouseEvent e, int lastx, int lasty) {
		xLeft = e.getX() + lastx;
		yTop = e.getY() + lasty;

	}

	/**
	 * Returns the x coordinate
	 * @return the x coordinate 
	 */
	public int getX() {
		return xLeft;
	}

	/**
	 * Returns the y coordinate
	 * @return the y coordinate
	 */
	public int getY() {
		return yTop;
	}

	/* 
	 * Normal draw method with black color
	 */
	public void draw(Graphics2D g2, int x, int y) {
		xLeft = x;
		yTop = y;
		if (trailer != null)
			trailer.draw(g2, x + 64, yTop);
		draw(g2);
	}

	/* 
	 * Recursive draw but with red color
	 */
	public void drawRedRecur(Graphics2D g2, int x, int y) {
		xLeft = x;
		yTop = y;
		if (trailer != null)
			trailer.drawRedRecur(g2, x + 64, yTop);
		drawRed(g2);
	}

	/**
	 * After car is removed from truck then this method will 
	 * make sure the car won't be painted again
	 */
	public void disappear() {
		disappear = true;
	}

	/**
	 * Recursive draw method to draw this car and the trailer
	 * @param g2 the Graphics2D variable to draw on
	 */
	public void drawRecursive(Graphics2D g2) {
		//if trailer exists then recurive draw
		if (trailer != null) {
			if (selected)
				trailer.drawRedRecur(g2, xLeft + 64, yTop);
			else
				trailer.draw(g2, xLeft + 64, yTop);
		}
		//if car is selected then do recursive draw but with red color
		if (selected)
			drawRed(g2);
		else
			draw(g2);

	}

	/**
	 * Draws the car and all of it's features
	 * @param g2 the Graphics2D variable to draw on
	 */
	public void drawRed(Graphics2D g2) {

		g2.setColor(Color.red);
		boundingBox = new Rectangle(xLeft, yTop, 64, 30);
		Rectangle body = new Rectangle(xLeft, yTop + 10, 60, 10);
		Ellipse2D.Double frontTire = new Ellipse2D.Double(xLeft + 10,
				yTop + 20, 10, 10);
		Ellipse2D.Double rearTire = new Ellipse2D.Double(xLeft + 40, yTop + 20,
				10, 10);

		Line2D.Double hitch = new Line2D.Double(xLeft + 60, yTop + 15,
				xLeft + 64, yTop + 15);

		// The bottom of the front windshield
		Point2D.Double r1 = new Point2D.Double(xLeft + 10, yTop + 10);
		// The front of the roof
		Point2D.Double r2 = new Point2D.Double(xLeft + 20, yTop);
		// The rear of the roof
		Point2D.Double r3 = new Point2D.Double(xLeft + 40, yTop);
		// The bottom of the rear windshield
		Point2D.Double r4 = new Point2D.Double(xLeft + 50, yTop + 10);

		Line2D.Double frontWindshield = new Line2D.Double(r1, r2);
		Line2D.Double roofTop = new Line2D.Double(r2, r3);
		Line2D.Double rearWindshield = new Line2D.Double(r3, r4);

		g2.drawString(Integer.toString(number), xLeft + 30, yTop + 20);
		g2.draw(body);
		g2.draw(frontTire);
		g2.draw(rearTire);
		g2.draw(frontWindshield);
		g2.draw(roofTop);
		g2.draw(rearWindshield);
		g2.draw(hitch);
		// g2.draw(boundingBox);
		g2.setColor(Color.black);
	}

	/**
	 * Add a car to the trailer of this car
	 * @param selectedCar The car to add to the trailer of this car
	 */
	public void addLast(Car selectedCar) {
		
		//if not trailer then add car to trailer
		if (trailer == null) {
			trailer = selectedCar;
			return;
		}
		//if there is already a trailer then add to the last car
		Car temp = (Car) trailer;
		while (temp.trailer != null)
			temp = (Car) temp.trailer;
		temp.trailer = selectedCar;

	}

}
