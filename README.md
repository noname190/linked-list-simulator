# README #

### What is this repository for? ###

* This is a linked list simulator
* Was created for school assignment
* Learned about Menu bars, action listeners, linked lists, inheritance, swing/shapes

### How do I get set up? ###

Compile and run SimulatorViewer.java

Start off the program by clicking on the screen 6 times to spawn the vehicles. 

You can then drag and drop the cars. Manipulate them using the List menubar action.

You can move the truck by using the "Edit" menubar action.